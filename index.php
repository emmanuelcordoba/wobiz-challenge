<?php

if(!file_exists('hormigas.json'))
{
    echo 'No existe archivo';
    exit();
}

$hormigas = json_decode(file_get_contents('hormigas.json'));

echo 'Primera generación:'.PHP_EOL;
showArray($hormigas);
echo "\n";

$cantidades = [];

for($fila = 0; $fila < count($hormigas); $fila++)
{    
    for($columna = 0; $columna < count($hormigas[$fila]); $columna++)
    {
        $cantidades[$fila][$columna] = contarVecinas($hormigas,$fila,$columna);
    }
}

for($fila = 0; $fila < count($hormigas); $fila++)
{    
    for($columna = 0; $columna < count($hormigas[$fila]); $columna++)
    {
        if($hormigas[$fila][$columna])
        {
            $hormigas[$fila][$columna] = $cantidades[$fila][$columna] == 2 |  $cantidades[$fila][$columna] == 3 ? 1 : 0;
        } else {
            $hormigas[$fila][$columna] = $cantidades[$fila][$columna] == 3 ? 1 : 0;
        }
    }
}
echo 'Segunda generación:'.PHP_EOL;;
showArray($hormigas);


function contarVecinas($arr, $f, $c)
{
    $cant = 0;
    $f_min = $f>0 ? $f-1 : $f;
    $c_min = $c>0 ? $c-1 : $c;    
    $f_max = isset($arr[$f+1][$c]) ? $f+1 : $f;
    $c_max = isset($arr[$f][$c+1]) ? $c+1 : $c;    
    
    if($f > $f_min)
    {
        $length = $c_max - $c_min + 1;
        $cant += array_sum(array_slice($arr[$f-1],$c_min,$length));
    }
    if($f < $f_max)
    {
        $length = $c_max - $c_min + 1;
        $cant += array_sum(array_slice($arr[$f+1],$c_min,$length));
    }
    if($c > $c_min)
    {
        $cant += $arr[$f][$c-1];
    }
    if($c < $c_max)
    {
        $cant += $arr[$f][$c+1];
    }
    return $cant;    
}

function showArray($arr)
{
    foreach($arr as $row)
    {
        foreach($row as $cell)
        {
            echo $cell.' ';
        }
        echo PHP_EOL;
    }    
}
